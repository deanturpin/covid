import json

# Open input data
f = open("tmp/uk.json")
j = json.load(f)

# Open output data
cases030 = open("tmp/cases-030-days.csv", "w+")
cases060 = open("tmp/cases-060-days.csv", "w+")
cases120 = open("tmp/cases-120-days.csv", "w+")
cases240 = open("tmp/cases-240-days.csv", "w+")
cases480 = open("tmp/cases-480-days.csv", "w+")
casesall = open("tmp/cases-all.csv", "w+")

total_reports = len(j["data"])
print(f"Total reports: {total_reports}\n")

for i in range(total_reports):

    # Results for the day
    cases_daily = j["data"][i]["latestBy"]

    # Store if within sub-range
    if i < 30: cases030.write(str(cases_daily) + "\n")
    if i < 60: cases060.write(str(cases_daily) + "\n")
    if i < 120: cases120.write(str(cases_daily) + "\n")
    if i < 240: cases240.write(str(cases_daily) + "\n")
    if i < 480: cases480.write(str(cases_daily) + "\n")

    # Store everything
    casesall.write(str(cases_daily) + "\n")

